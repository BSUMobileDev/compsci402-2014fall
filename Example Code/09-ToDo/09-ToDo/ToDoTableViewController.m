//
//  ToDoTableViewController.m
//  09-ToDo
//
//  Created by Michael Ziray on 11/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "ToDoTableViewController.h"
#import "DetailsViewController.h"
#import "ToDo.h"
#import "DatabaseController.h"

@interface ToDoTableViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *todoArray;
@end

@implementation ToDoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerClass: [UITableViewCell class]
           forCellReuseIdentifier: @"reuseIdentifier"];
    
    [self.navigationItem setRightBarButtonItem: [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemAdd
                                                                                              target: self
                                                                                              action: @selector(addToDo) ]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName: @"ToDo"];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending: YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    NSError *error;
    NSArray *resultsArray = [[DatabaseController managedObjectContext] executeFetchRequest: fetchRequest error: &error];
    self.todoArray = [resultsArray mutableCopy];
    
    if( error != nil )
    {
        NSLog(@"Some error occurred %@", [error description]);
        NSLog(@"Some error occurred %@", error); // same as above
    }
    
    [self.tableView reloadData];
}

-(void)addToDo
{
    NSLog(@"laksjdf");
    [self showDetailsViewController: nil];
}
     
     

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.todoArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    
    ToDo *currentTodo = [self.todoArray objectAtIndex: indexPath.row];
    // Configure the cell...
    cell.textLabel.text = currentTodo.title;
    cell.detailTextLabel.text = currentTodo.category.categoryName;
//    cell.imageView.image = [[UIImage imageNamed:<#(NSString *)#>]]
    return cell;
}

- (void)showDetailsViewController:(ToDo *)todo
{
    DetailsViewController *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier: NSStringFromClass([DetailsViewController class]) ];
    
    if( todo != nil )
    {
        detailsViewController.currentToDo = todo;
    }
    
    [self.navigationController pushViewController: detailsViewController animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    [self showDetailsViewController: [self.todoArray objectAtIndex: indexPath.row]];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        ToDo *removedToDo = [self.todoArray objectAtIndex: indexPath.row];
        
        [self.todoArray removeObjectAtIndex: indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [[DatabaseController managedObjectContext] deleteObject: removedToDo];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
