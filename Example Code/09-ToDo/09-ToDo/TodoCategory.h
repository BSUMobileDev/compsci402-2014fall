//
//  TodoCategory.h
//  09-ToDo
//
//  Created by Michael Ziray on 11/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ToDo;

@interface TodoCategory : NSManagedObject

@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSSet *todos;
@end

@interface TodoCategory (CoreDataGeneratedAccessors)

- (void)addTodosObject:(ToDo *)value;
- (void)removeTodosObject:(ToDo *)value;
- (void)addTodos:(NSSet *)values;
- (void)removeTodos:(NSSet *)values;

@end
