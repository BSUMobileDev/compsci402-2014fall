//
//  DetailsViewController.h
//  09-ToDo
//
//  Created by Michael Ziray on 11/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDo.h"

@interface DetailsViewController : UIViewController

@property(nonatomic, strong)ToDo *currentToDo;

@end
