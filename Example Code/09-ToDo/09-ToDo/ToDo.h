//
//  ToDo.h
//  09-ToDo
//
//  Created by Michael Ziray on 11/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TodoCategory.h"

@interface ToDo : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * isComplete;
@property (nonatomic, retain) NSDate * dueDate;
@property (nonatomic, retain) TodoCategory *category;

@end
