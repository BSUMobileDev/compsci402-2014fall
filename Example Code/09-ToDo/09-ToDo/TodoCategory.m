//
//  TodoCategory.m
//  09-ToDo
//
//  Created by Michael Ziray on 11/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "TodoCategory.h"
#import "ToDo.h"


@implementation TodoCategory

@dynamic categoryName;
@dynamic todos;

@end
