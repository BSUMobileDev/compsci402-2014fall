//
//  ToDo.m
//  09-ToDo
//
//  Created by Michael Ziray on 11/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "ToDo.h"


@implementation ToDo

@dynamic title;
@dynamic isComplete;
@dynamic dueDate;
@dynamic category;

@end
