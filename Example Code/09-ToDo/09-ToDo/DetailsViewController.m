//
//  DetailsViewController.m
//  09-ToDo
//
//  Created by Michael Ziray on 11/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "DetailsViewController.h"
#import "DatabaseController.h"


@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITextField *toDoTitle;
@property (weak, nonatomic) IBOutlet UITextField *category;

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if( self.currentToDo == nil )
    {
        NSLog(@"no TODO");
        self.currentToDo = [NSEntityDescription insertNewObjectForEntityForName: NSStringFromClass([ToDo class])
                                                         inManagedObjectContext: [DatabaseController managedObjectContext]];
    }
    else
    {
        self.toDoTitle.text = self.currentToDo.title;
        self.category.text  = self.currentToDo.category.categoryName;
//        self.datePicker.date = self.currentToDo.dueDate;
    }
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemSave target: self action:@selector(save)];
    [self.navigationItem setRightBarButtonItem: saveButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)save
{
    self.currentToDo.title = self.toDoTitle.text;
    self.currentToDo.category.categoryName = self.category.text;
    
    [self.navigationController popViewControllerAnimated: YES];
    
    [DatabaseController save];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
