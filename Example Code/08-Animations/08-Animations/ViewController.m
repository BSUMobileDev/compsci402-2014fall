//
//  ViewController.m
//  08-Animations
//
//  Created by Michael Ziray on 10/29/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *animatedView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self animateView];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)animateView
{
    CGRect originalFrame = _animatedView.frame;
    CGRect newFrame = _animatedView.frame;
    newFrame.size.height -= 400;
    
    [UIView animateWithDuration: 1.0 animations:^(void){
        self.animatedView.frame = newFrame;
        self.animatedView.backgroundColor = [UIColor redColor];
        self.animatedView.alpha =0.5f;
    } completion:^(BOOL finished){
        self.animatedView.frame = originalFrame;
        self.animatedView.backgroundColor = [UIColor blackColor];
        self.animatedView.alpha =1.0f;
    }
                     ];
}

@end
