//
//  PopoverViewController.h
//  04-Camera
//
//  Created by Michael Ziray on 9/15/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end
