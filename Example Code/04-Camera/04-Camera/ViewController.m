//
//  ViewController.m
//  04-Camera
//
//  Created by Michael Ziray on 9/15/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "ViewController.h"
#import "PopoverViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property(nonatomic, strong)UIImagePickerController *imagePickerController;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (IBAction)launchCamera:(id)sender
{
    PopoverViewController *popoverViewController = [[PopoverViewController alloc] initWithNibName:@"PopoverViewController" bundle: nil];
    
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController: popoverViewController];
    
    [popoverController presentPopoverFromRect: self.cameraButton.frame
                                       inView: self.view
                     permittedArrowDirections: UIPopoverArrowDirectionUp
                                     animated: YES];
}




    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    
    
    
    
    
    
    
@end
