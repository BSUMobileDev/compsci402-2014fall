//
//  PopoverViewController.m
//  04-Camera
//
//  Created by Michael Ziray on 9/15/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "PopoverViewController.h"

@interface PopoverViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property(strong, nonatomic)UIImagePickerController *imagePickerController;
@end

@implementation PopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    self.imagePickerController = [[UIImagePickerController alloc] init];
    
    if( [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else{
        _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    _imagePickerController.delegate = self;
    
    [self.view addSubview: _imagePickerController.view];
}


-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [_imagePickerController.view removeFromSuperview];
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    self.imageView.image = image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
