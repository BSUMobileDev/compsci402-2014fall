//
//  BuildingsTableViewController.m
//  06-TableViews
//
//  Created by Michael Ziray on 10/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "BuildingsTableViewController.h"
#import "DatabaseController.h"
#import "Building.h"

@interface BuildingsTableViewController ()

@property(nonatomic, strong)NSArray *mockArray;
@property(nonatomic, strong)NSFetchedResultsController *fetchedRequestsController;
@property(nonatomic, strong)NSFetchRequest *fetchRequest;
@end

@implementation BuildingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    
    self.fetchRequest = [[NSFetchRequest alloc] initWithEntityName: NSStringFromClass([Building class])];
    
    [self.fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"buildingName" ascending:YES]]];
    
    
    NSError *error;
    
    self.fetchedRequestsController = [[NSFetchedResultsController alloc] initWithFetchRequest: self.fetchRequest managedObjectContext: [DatabaseController managedObjectContext] sectionNameKeyPath: nil cacheName: nil];
    [self.fetchedRequestsController performFetch: &error];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self.fetchedRequestsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.

    NSArray *sections = [self.fetchedRequestsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"Cell" forIndexPath:indexPath];
    
    [[DatabaseController managedObjectContext] executeFetchRequest: self.fetchRequest error: nil];
    Building *currentBuilding = [self.fetchedRequestsController objectAtIndexPath: indexPath];
    NSString *title = currentBuilding.buildingName;
    
    // Configure the cell...
    cell.textLabel.text = title;
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@", indexPath);
    
    [tableView deselectRowAtIndexPath: indexPath animated: NO];
    return;
}





@end
