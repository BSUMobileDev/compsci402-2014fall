//
//  BuildingsTableViewController.h
//  06-TableViews
//
//  Created by Michael Ziray on 10/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuildingsTableViewController : UITableViewController

@end
