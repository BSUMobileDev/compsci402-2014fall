//
//  Buidling.h
//  06-TableViews
//
//  Created by Michael Ziray on 10/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Building : NSManagedObject

@property (nonatomic, retain) NSString * buildingName;
@property (nonatomic, retain) NSString * department;

@end
