//
//  StudentPhotoMeta.h
//  05-CoreData
//
//  Created by Michael Ziray on 10/6/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class StudentPhoto;

@interface StudentPhotoMeta : NSManagedObject

@property (nonatomic, retain) StudentPhoto *studentPhoto;

@end
