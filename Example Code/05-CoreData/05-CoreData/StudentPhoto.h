//
//  StudentPhoto.h
//  05-CoreData
//
//  Created by Michael Ziray on 10/6/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Student;

@interface StudentPhoto : NSManagedObject

@property (nonatomic, retain) NSManagedObject *photoMetaData;
@property (nonatomic, retain) Student *student;

@end
