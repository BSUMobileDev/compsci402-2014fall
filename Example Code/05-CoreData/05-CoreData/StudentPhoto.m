//
//  StudentPhoto.m
//  05-CoreData
//
//  Created by Michael Ziray on 10/6/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "StudentPhoto.h"
#import "Student.h"


@implementation StudentPhoto

@dynamic photoMetaData;
@dynamic student;

@end
