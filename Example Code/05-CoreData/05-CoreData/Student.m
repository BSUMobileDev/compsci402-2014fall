//
//  Student.m
//  05-CoreData
//
//  Created by Michael Ziray on 10/6/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "Student.h"
#import "Courses.h"


@implementation Student

@dynamic firstName;
@dynamic lastName;
@dynamic courses;

@end
