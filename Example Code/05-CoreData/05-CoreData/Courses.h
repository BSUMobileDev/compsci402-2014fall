//
//  Courses.h
//  05-CoreData
//
//  Created by Michael Ziray on 10/6/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Student;

@interface Courses : NSManagedObject

@property (nonatomic, retain) NSString * courseName;
@property (nonatomic, retain) Student *students;

@end
