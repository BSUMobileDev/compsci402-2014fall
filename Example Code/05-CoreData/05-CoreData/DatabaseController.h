//
//  DatabaseController.h
//  AURMA
//
//  Created by Drake on 3/11/14.
//  Copyright (c) 2014 LiftSquad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

static NSManagedObjectContext *managedObjectContext;
static NSPersistentStoreCoordinator *persistentStoreCoordinator;
static NSManagedObjectModel *managedObjectModel;

@interface DatabaseController : NSObject

+ (NSManagedObjectContext *) managedObjectContext;

+(void)save;

@end
