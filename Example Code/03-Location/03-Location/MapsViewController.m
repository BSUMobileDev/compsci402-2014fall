//
//  MapsViewController.m
//  03-Location
//
//  Created by Michael Ziray on 9/8/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "MapsViewController.h"

#import "LocationController.h"
#import <MapKit/MapKit.h>
#import "BuildingsController.h"
#import "AFNetworking.h"


@interface MapsViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(locationDidUpdate:) name: kLocationDidChange object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(buildingsChange) name: BUILDINGS_CHANGED_NOTIFICATION object: nil];
    
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager GET:@"http://zstudiolabs.com/labs/compsci402/buildings.json"
//      parameters:nil
//         success:^(AFHTTPRequestOperation *operation, id responseObject) {
//             NSLog(@"JSON: %@", responseObject);
//             NSArray *buildingsArray = (NSArray *)responseObject;
//             
//             for (NSDictionary *currentBuilding in buildingsArray)
//             {
//                 Building *newBuilding = [[Building alloc] init];
//                 
//                 newBuilding.buildingName = currentBuilding[BUILDING_NAME];
//                 newBuilding.buildingDescription = [currentBuilding objectForKey:@"description"];
//                 
//                 NSDictionary *locationInfo = currentBuilding[BUILDING_LOCATION];
//                 newBuilding.buildingLocation = CLLocationCoordinate2DMake([locationInfo[@"latitude"] doubleValue], [locationInfo[@"longitude"] doubleValue]);
//             }
//         }
//         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//             NSLog(@"Error: %@", error);
//         }
//     ];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    NSArray *buildings = [BuildingsController buildings];
    
//    [BuildingsController deleteAllBuildings];
//    [NSString pathWithComponents: buildings];
}


-(void)locationDidUpdate:(NSNotification *)notification
{
    NSLog(@"%@", [notification object]);
    
    CLLocation *currentLocation = (CLLocation *)[notification object];
    
    MKPointAnnotation *currentLocationAnnotation = [[MKPointAnnotation alloc] init];
    currentLocationAnnotation.title = @"hello";
    currentLocationAnnotation.subtitle = @"subtitle";
    currentLocationAnnotation.coordinate = currentLocation.coordinate;
    
    [self.mapView addAnnotation: currentLocationAnnotation];
    
}

-(void)buildingsChange
{
    NSLog(@"maps vc was notified that the buildings changed");
    
//    NSArray *buildings = [BuildingsController buildings];
    
    
//    for (Building *currentBuilding in buildings)
//    {
//        [self.mapView addAnnotation: currentBuilding.buildingPin];
//    }

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end




