//
//  BuildingsFactory.h
//  03-Location
//
//  Created by Michael Ziray on 9/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Building.h"


@interface BuildingsFactory : NSObject

+(Building *)createBuildingWithName:(NSString *)name description:(NSString *)desciption location:(CLLocationCoordinate2D)location;

+(NSMutableArray *)createBuildingsFromRAW:(NSArray *)buildingsRAW;
@end
