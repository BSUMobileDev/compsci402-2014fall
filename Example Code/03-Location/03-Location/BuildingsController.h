//
//  BuildingsController.h
//  03-Location
//
//  Created by Michael Ziray on 9/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Building.h"

#define BUILDINGS_CHANGED_NOTIFICATION @"BUILDINGDS_CHANGED_NOTIFICATION"


@interface BuildingsController : NSObject

+(NSArray *)buildings;
+(void)addBuilding:(Building *)newBuilding;
+(void)deleteAllBuildings;

@end
