//
//  BuildingsFactory.m
//  03-Location
//
//  Created by Michael Ziray on 9/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "BuildingsFactory.h"

@implementation BuildingsFactory

+(Building *)createBuildingWithName:(NSString *)name description:(NSString *)description location:(CLLocationCoordinate2D)location
{
    Building *building = [[Building alloc] init];
    building.buildingName = name;
    building.buildingDescription = description;
    building.buildingLocation = location;
    
    return building;
}

+(NSMutableArray *)createBuildingsFromRAW:(id)buildingsRAW
{
    NSArray *buildingsRAWArray = (NSArray *)buildingsRAW;
    
    NSMutableArray *buildingsArray = [[NSMutableArray alloc] init];
    Building *newBuilding;
    for (NSDictionary *buildingRAWInfo in buildingsRAWArray)
    {
        newBuilding = [[Building alloc] init];
        newBuilding.buildingName = [buildingRAWInfo objectForKey: @"name"];
//        newBuilding.buildingPin = [[MKPoin]]
        [buildingsArray addObject: newBuilding];
    }
    
    return buildingsArray;
}


@end
