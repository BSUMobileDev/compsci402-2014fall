//
//  DetailsViewController.m
//  03-Location
//
//  Created by Michael Ziray on 9/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "DetailsViewController.h"

#import "BuildingsController.h"
#import "BuildingsFactory.h"
#import "LocationController.h"


@interface DetailsViewController()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *detailsTextField;

@end


@implementation DetailsViewController
- (IBAction)saveLocation:(id)sender
{
    NSString *name = [self.nameTextField text];
    NSString *description = [self.detailsTextField text];
    
    
    [BuildingsController addBuilding: [BuildingsFactory createBuildingWithName: name
                                                                   description: description
                                                                      location: [[LocationController currentLocation] coordinate]]];
    
    [self dismissViewControllerAnimated: YES
                             completion: nil];
    
}





@end




