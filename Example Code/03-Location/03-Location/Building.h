//
//  Building.h
//  03-Location
//
//  Created by Michael Ziray on 9/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

#define BUILDING_NAME @"name"
#define BUILDING_LOCATION @"location"


@interface Building : NSObject

@property(nonatomic, copy) NSString *buildingName;
@property(nonatomic, copy) NSString *buildingDescription;
@property(nonatomic, assign) CLLocationCoordinate2D buildingLocation;

//@property(nonatomic, strong)MKPointAnnotation *buildingPin;
@end
