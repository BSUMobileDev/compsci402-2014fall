//
//  BuildingsController.m
//  03-Location
//
//  Created by Michael Ziray on 9/17/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "BuildingsController.h"
#import "BuildingsFactory.h"

#import "AFNetworking.h"
static NSMutableArray *buildingsArray = nil;




@implementation BuildingsController


+(NSArray *)buildings
{
    [BuildingsController initializeBuildingsArray];
    
    return [buildingsArray copy];
}


+(void)addBuilding:(Building *)newBuilding
{
    [BuildingsController initializeBuildingsArray];
    [buildingsArray addObject: newBuilding];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: BUILDINGS_CHANGED_NOTIFICATION object:nil];
}

+(void)deleteAllBuildings
{
    buildingsArray = nil;
}


+(void)initializeBuildingsArray
{
    if( buildingsArray == nil )
    {
        buildingsArray = [[NSMutableArray alloc] init];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:@"http://zstudiolabs.com/labs/compsci402/buildings.json"
          parameters:nil
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSLog(@"JSON: %@", responseObject);
                 [buildingsArray addObjectsFromArray:[BuildingsFactory createBuildingsFromRAW: responseObject]];
                 [[NSNotificationCenter defaultCenter] postNotificationName: BUILDINGS_CHANGED_NOTIFICATION object:nil];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
             }
         ];
    }
}

@end
