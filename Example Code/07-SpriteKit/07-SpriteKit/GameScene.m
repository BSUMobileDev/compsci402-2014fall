//
//  GameScene.m
//  07-SpriteKit
//
//  Created by Michael Ziray on 10/15/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "GameScene.h"

static const uint32_t contactCategory     = 0x1;


@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    myLabel.text = @"Hello, World!";
    myLabel.fontSize = 45;
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame));

    
    
    [self addChild:myLabel];
 
    [self.physicsWorld setContactDelegate: self];
    
    SKNode *spaceship = [self childNodeWithName:@"Spaceship"];
    spaceship.physicsBody.contactTestBitMask = contactCategory;
    spaceship.physicsBody.categoryBitMask = contactCategory;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship"];

        sprite.xScale = 0.5;
        sprite.yScale = 0.5;
        sprite.position = location;
        
        SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
        
        [sprite runAction:[SKAction repeatActionForever:action]];
        
        [self addChild:sprite];
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    SKNode *spaceship = [self childNodeWithName:@"Spaceship"];
//    spaceship.alpha = 0;
}


-(void)didBeginContact:(SKPhysicsContact *)contact
{
    NSLog(@"contact: %@", contact);
}






@end
