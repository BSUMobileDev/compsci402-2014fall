//
//  GameScene.h
//  07-SpriteKit
//

//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>

@end
