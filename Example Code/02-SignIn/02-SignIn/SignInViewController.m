//
//  SignInViewController.m
//  02-SignIn
//
//  Created by Michael Ziray on 9/3/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "SignInViewController.h"
#import "JNKeychain.h"

#define USERNAME_KEY @"USERNAME"
#define PASSWORD_KEY @"PASSWORD"

@interface SignInViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Sign in view controller did fire");
    
    NSString *username = [JNKeychain loadValueForKey: USERNAME_KEY]; // [[NSUserDefaults standardUserDefaults] objectForKey:@"USERNAME_KEY"];
    NSString *password = [JNKeychain loadValueForKey: PASSWORD_KEY]; //[[NSUserDefaults standardUserDefaults] objectForKey:@USERNAME_KEY];
    if( username != nil )
    {
        self.usernameTextField.text = username;
    }
    
    if( password != nil )
    {
        [[self passwordTextField] setText: password];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)signIn:(id)sender
{
    NSLog(@"Sign in method fired");
    
    NSString *username = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;
    
    NSLog(@"username: %@\npassword: %@", username, password);
    
    // VERY INSECURE!!
//    [[NSUserDefaults standardUserDefaults] setObject: username forKey: @"USERNAME"];
//    [[NSUserDefaults standardUserDefaults] setObject: password forKey: @"PASSWORD"];
    [JNKeychain saveValue: username forKey: USERNAME_KEY];
    [JNKeychain saveValue: password forKey: PASSWORD_KEY];
    
    [self dismissViewControllerAnimated: YES
                             completion: nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
