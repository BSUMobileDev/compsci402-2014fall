//
//  RootViewController.m
//  02-SignIn
//
//  Created by Michael Ziray on 11/12/14.
//  Copyright (c) 2014 Z Studio Labs. All rights reserved.
//

#import "RootViewController.h"
#import "SignInViewController.h"


@interface RootViewController ()
@property(nonatomic, strong)SignInViewController *signInViewController;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@end


@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString: @"http://www.google.com"];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL: url];
    [_webView loadRequest: urlRequest];
    
    [self.urlTextField addTarget: self
                          action: @selector(loadARequest) forControlEvents:UIControlEventEditingDidEnd];
    
    [self.datePicker addTarget: self
                        action: @selector(dateDidChange) forControlEvents:UIControlEventValueChanged];
}

-(void)loadARequest
{
    NSURL *url = [NSURL URLWithString: self.urlTextField.text];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL: url];
    [_webView loadRequest: urlRequest];
}


-(void)dateDidChange
{
    NSLog(@"%@", self.datePicker.date);
}

-(void)viewDidAppear:(BOOL)animated
{
    if( self.signInViewController == nil )
    {
        self.signInViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
        //    [self setSignInViewController: [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];]; // same as above statement
    
        [self presentViewController: _signInViewController
                           animated: YES
                         completion: nil];
    }
}


-(IBAction)logOut:(id)sender
{
    [self presentViewController: _signInViewController
                       animated: YES
                     completion: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 4;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return @"Row Title";
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}
@end
